<?php

require '../../../../vendor/autoload.php';

use gamepedia\model\DBConnection;
use gamepedia\controler\ControlerGames;


$app = new \Slim\Slim();

$app->response->headers->set('Content-Type', 'application/json');


$app->get('/css',function(){})->name('css');

$app->get('/', function(){
	(new ControlerGames())->index();	
})->name('racine');

$app->get('/api', function(){
	(new ControlerGames())->index();
})->name('api');

$app->get('/api/games', function(){
	(new ControlerGames())->allGames();
})->name('games');

$app->get('/api/games/:id', function($id){
	(new ControlerGames())->getGameById($id);
})->name('gameById');


$app->get('/api/games/:id/comments', function($id){
	(new ControlerGames())->getGameCommentsById($id);
})->name('gameCommentsById');


$app->get('/api/games/:id/characters', function($id){
	(new ControlerGames())->getGameCharactersById($id);
})->name('gameCharactersById');

$app->run();