<?php

namespace gamepedia\controler;

use gamepedia\model\DBConnection;
use gamepedia\model\Game;
use gamepedia\model\Commentaire;
use gamepedia\model\Utilisateur;

DBConnection::getInstance();

class ControlerGames {
	
	private $page;
	private $page_next;
	private $page_prev;
	
	public function index() {
		echo "Index de l'API de Gamepedia";
	}
	
	public function allGames(){
		$app = \Slim\Slim::getInstance();
		if($app->request->params('page') != null){
			$page = $app->request->params('page');
			if($page < 1 || $page > 240){
				echo 'Erreur 404';
			}else{
				if($page == 1){
					$page_prev = 1;
					$page_next = 2;
				}elseif($page == 240){
					$page_prev = 239;
					$page_next = 240;
				}else{
					$page_prev = $page - 1;
					$page_next = $page + 1;
				}
			}
		}else{
			$page = 1;
			$page_prev = 1;
			$page_next = 2;
		}
		$games = Game::select("id", "name", "alias", "deck")->offset(($page-1)*200)->take(200)->get();
		$list = array();
		foreach ($games as $g){
			$elem = array("game" => $g, "links" => array(
												"self" => array("href" => $app->urlFor('gameById', ['id' => $g->id]))
											 ));
			array_push($list, $elem);
		}
		 $json = array("games" => $list,
							   "prev" => array("href" => $app->urlFor('games') . "?page=" . $page_prev), 
							   "next" => array("href" => $app->urlFor('games') . "?page=" . $page_next)
				
		);
		 
		echo(json_encode($json));
	}


	public function getGameById($id){
		$app = \Slim\Slim::getInstance();
		$game = Game::select("id", "name", "alias", "deck", "description", "original_release_date")->where("id", "=", $id)->first();

		//Ajout des plateformes
		$tab_plateforme = [ "id" => $game->platform[0]->id, 
							"alias" => $game->platform[0]->alias,
							"abbreviation" => $game->platform[0]->abbreviation,
							"description" => $game->platform[0]->description
		];
	
		//Ajout des links
		$json = [ "game" => $game, "links" => [ 
											"comments" => [ "href" => $app->urlFor('gameCommentsById', ['id' => $game->id])],
											"characters" => [ "href" =>$app->urlFor('gameCharactersById', ['id' => $game->id])]
											 ]			
		];
		
		echo(json_encode($json));
	}

	
	public function getGameCommentsById($id){
		$comm = Commentaire::select("id", "titre", "contenu", "date_creation", "id_utilisateur")
						  ->where("id_game", "=", $id)
						  ->first();

		$tmp = json_decode($comm, true);
		array_push($tmp, $comm->utilisateur->nom);

		echo json_encode($tmp);
	}
	
		
	
}