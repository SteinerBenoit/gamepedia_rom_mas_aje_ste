<?php

use gamepedia\model\DBConnection;
use gamepedia\model\Game;

require '../../../../vendor/autoload.php';

DBConnection::getInstance();

//jeux commencant par FIFA
$time_start = microtime(true);
$g = Game::where( 'name', 'like', 'FIFA%' )->get();

foreach($g as $item){
	$p = $item->personnage()->get();
}

$time_end = microtime(true);
$time=$time_end - $time_start;

echo "Temps d'execution pour les jeux commencant par FIFA: $time secondes <br><br>";



//jeux commencant par Premier
$time_start = microtime(true);
$g = Game::where( 'name', 'like', 'Premier%' )->get();

foreach($g as $item){
	$p = $item->personnage()->get();
}

$time_end = microtime(true);
$time=$time_end - $time_start;

echo "Temps d'execution pour les jeux commencant par Premier: $time secondes <br><br>";



//jeux commencant par Land
$time_start = microtime(true);
$g = Game::where( 'name', 'like', 'Land%' )->get();

foreach($g as $item){
	$p = $item->personnage()->get();
}

$time_end = microtime(true);
$time=$time_end - $time_start;

echo "Temps d'execution pour les jeux commencant par Land: $time secondes <br><br>";



/*
1ère fois:

Temps d'execution pour les jeux commencant par FIFA: 2.3135969638824 secondes

Temps d'execution pour les jeux commencant par Premier: 0.777508020401 secondes

Temps d'execution pour les jeux commencant par Land: 0.96853399276733 secondes



Autres fois:

Temps d'execution pour les jeux commencant par FIFA: 1.6587529182434 secondes

Temps d'execution pour les jeux commencant par Premier: 0.74710988998413 secondes

Temps d'execution pour les jeux commencant par Land: 0.82498598098755 secondes 


*/




//----------------------------




//jeux contenant FIFA
$time_start = microtime(true);
$g = Game::where( 'name', 'like', '%FIFA%' )->get();

foreach($g as $item){
	$p = $item->personnage()->get();
}

$time_end = microtime(true);
$time=$time_end - $time_start;

echo "Temps d'execution pour les jeux contenant FIFA: $time secondes <br><br>";




//jeux contenant Premier
$time_start = microtime(true);
$g = Game::where( 'name', 'like', '%Premier%' )->get();

foreach($g as $item){
	$p = $item->personnage()->get();
}

$time_end = microtime(true);
$time=$time_end - $time_start;

echo "Temps d'execution pour les jeux contenant Premier: $time secondes <br><br>";



//jeux contenant Land
$time_start = microtime(true);
$g = Game::where( 'name', 'like', '%Land%' )->get();

foreach($g as $item){
	$p = $item->personnage()->get();
}

$time_end = microtime(true);
$time=$time_end - $time_start;

echo "Temps d'execution pour les jeux contenant Land: $time secondes <br><br>";


//cela englobe plus de resultats

/*
 1ère fois:

Temps d'execution pour les jeux contenant FIFA: 3.4720630645752 secondes

Temps d'execution pour les jeux contenant Premier: 1.8941738605499 secondes

Temps d'execution pour les jeux contenant Land: 25.495354890823 secondes 



 Autres fois:

Temps d'execution pour les jeux contenant FIFA: 2.3271210193634 secondes

Temps d'execution pour les jeux contenant Premier: 1.8270518779755 secondes

Temps d'execution pour les jeux contenant Land: 22.40638589859 secondes 

 */
