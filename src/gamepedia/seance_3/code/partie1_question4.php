<?php

use gamepedia\model\DBConnection;
use gamepedia\model\Game;

require '../../../../vendor/autoload.php';

DBConnection::getInstance();

// les jeux dont le nom débute par Mario et dont le rating initial contient "3+"
$time_start = microtime(true);
$g = Game::where( 'name', 'like', 'Mario%' )->get();

foreach($g as $item){
	$gr = $item->gameRating()->where('name','like','%3+%')->get();
	//foreach($gr as $item2){
	//	echo($item->name . ' || ' . $item2->name . '<br>');
	//}
}

$time_end = microtime(true);
$time=$time_end - $time_start;

echo "Temps d'execution: $time secondes <br><br>";


/*
 * 1ère fois : 1.66s
 * autres fois: 1.45s
 * 
 */

