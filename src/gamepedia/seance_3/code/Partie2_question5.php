<?php


use gamepedia\model\DBConnection;
use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\model\Compagnie;

require '../../../../vendor/autoload.php';

DBConnection::getInstance();

DB::enableQueryLog();

$c = Compagnie::where( 'name', 'like', '%Sony%' )->get();

foreach ( $c as $compagnie ){
	$game=$compagnie->game()->get();
	foreach($game as $g){
		echo($g->name . '<br>');
	}
}

$query = DB::getQueryLog();
$lastQuery = end($query);

var_dump($query);