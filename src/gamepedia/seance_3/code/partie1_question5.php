<?php

use gamepedia\model\DBConnection;
use gamepedia\model\Game;

require '../../../../vendor/autoload.php';

DBConnection::getInstance();

//jeux commencant par Mario
$time_start = microtime(true);
$g = Game::where( 'name', 'like', 'Mario%' )->get();

foreach($g as $item){
	$p = $item->personnage()->get();
}

$time_end = microtime(true);
$time=$time_end - $time_start;

echo "Temps d'execution pour les jeux commencant par Mario: $time secondes <br><br>";



//jeux commencant par Desert
$time_start = microtime(true);
$g = Game::where( 'name', 'like', 'Desert%' )->get();

foreach($g as $item){
	$p = $item->personnage()->get();
}

$time_end = microtime(true);
$time=$time_end - $time_start;

echo "Temps d'execution pour les jeux commencant par Desert: $time secondes <br><br>";



//jeux commencant par Dragon
$time_start = microtime(true);
$g = Game::where( 'name', 'like', 'Dragon%' )->get();

foreach($g as $item){
	$p = $item->personnage()->get();
}

$time_end = microtime(true);
$time=$time_end - $time_start;

echo "Temps d'execution pour les jeux commencant par Dragon: $time secondes <br><br>";



/*
1ère fois:

Temps d'execution pour les jeux commencant par Mario: 3.8359410762787 secondes

Temps d'execution pour les jeux commencant par Desert: 1.388680934906 secondes

Temps d'execution pour les jeux commencant par Dragon: 11.021665096283 secondes 



Autres fois:

Temps d'execution pour les jeux commencant par Mario: 3.6527960300446 secondes

Temps d'execution pour les jeux commencant par Desert: 1.1901218891144 secondes

Temps d'execution pour les jeux commencant par Dragon: 9.3920340538025 secondes


*/


