<?php
use gamepedia\model\DBConnection;
use gamepedia\model\Game;

require '../../../../vendor/autoload.php';


DBConnection::getInstance();

$time_start = microtime(true);

$list = Game::where( 'name', 'like', '%Mario%' )->get();

$time_end = microtime(true);
$time=$time_end - $time_start;

echo "Temps d'execution: $time secondes <br><br>";

foreach ( $list as $l ) {
	echo($l->name . "<br>");
}
