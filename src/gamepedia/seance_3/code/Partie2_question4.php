<?php


use gamepedia\model\DBConnection;
use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\model\Game;

require '../../../../vendor/autoload.php';

DBConnection::getInstance();

DB::enableQueryLog();

$g = Game::where( 'name', 'like', 'Mario%' )->get();

foreach ( $g as $game ){
	$pers=$game->personnage()->get();
	foreach($pers as $p){
		echo($p->name . '<br>');
	}
}

$query = DB::getQueryLog();
$lastQuery = end($query);

var_dump($query);
