<?php


use gamepedia\model\DBConnection;
use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\model\Game;

require '../../../../vendor/autoload.php';

DBConnection::getInstance();

DB::enableQueryLog();

$g = Game::where('name', 'like', 'Mario%' )->with('personnage')->get();

foreach($g as $game){
	foreach($game->personnage as $p){
		echo($p->name . "<br>");
	}
}


$query = DB::getQueryLog();

var_dump($query);
