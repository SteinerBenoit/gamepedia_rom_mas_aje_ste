<?php
use gamepedia\model\DBConnection;
use gamepedia\model\Game;

require '../../../../vendor/autoload.php';

DBConnection::getInstance();

// lister l'ensemble des jeux

$time_start = microtime(true);
$jeux = Game::get();
$time_end = microtime(true);
$time=$time_end - $time_start;
echo "Temps d'execution: $time secondes <br><br>";
