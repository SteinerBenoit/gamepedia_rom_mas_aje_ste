<?php

use gamepedia\model\DBConnection;
use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\model\Utilisateur;
use gamepedia\model\Commentaire;


require '../../../../vendor/autoload.php';

DBConnection::getInstance();

echo '<meta charset="UTF-8">';

DB::enableQueryLog();

$user= Utilisateur::find(19334);
$comment=$user->commentaire()->orderby('created_at','desc')->get();
foreach ($comment as $c){
	echo ($c->created_at . " : " .$c->contenu . '<br>');
}

$query = DB::getQueryLog();
$lastQuery = end($query);



