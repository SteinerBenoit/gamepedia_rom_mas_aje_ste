<?php

use gamepedia\model\DBConnection;
use gamepedia\model\Utilisateur;
use gamepedia\model\Commentaire;

require '../../../../vendor/autoload.php';

$faker = Faker\Factory::create('fr_FR');

echo '<meta charset="UTF-8">';

DBConnection::getInstance();


for ($i=0; $i < 25000; $i++) {
	$firstName = $faker->firstName;
	//echo $firstName . "<br>";
	
	$lastName = $faker->lastName;
	//echo $lastName . "<br>";
	
	$domain = $faker->domainName;
	$number = $faker->randomNumber(2);	
	$email = $firstName . "." . $lastName . "." . $number . "@" . $domain;	
	//echo $email . "<br>";
	
	$adress = $faker->address;
	//echo $adress . "<br>";
	
	$phoneNumber =  $faker->phoneNumber;
	//echo $phoneNumber . "<br>";
	
	$date = $faker->dateTime;
	//echo $date .  "<br><br>";
	
	Utilisateur::create(['nom' => $lastName, 'prenom' => $firstName, 'email' => $email, 'adresse_detaillee' => $adress, 'numero_tel' => $phoneNumber, 'date_naissance' => $date])->save();
}


echo 'Utilisateurs fait !<br><br>';




for ($i=0; $i < 250000; $i++) {
 $titre = $faker->sentence(5);
 //echo $titre . "<br>";

 $contenu = $faker->paragraph(4);
 //echo $contenu . "<br>";

 $date_creation = $faker->dateTime;
 //echo $date_creation . "<br>";

 $id_game = $faker->numberBetween(1, 47948) ;
 //echo $id_game . "<br>";

 $id_utilisateur =  $faker->numberBetween(1, 38081);
 //echo $id_utilisateur . "<br><br>";


 Commentaire::create(['titre' => $titre, 'contenu' => $contenu, 'date_creation' => $date_creation, 'id_game' => $id_game, 'id_utilisateur' => $id_utilisateur])->save();
 
}

echo 'Commentaires fait !<br><br>';
 