<?php

use gamepedia\model\DBConnection;
use gamepedia\model\Utilisateur;
use gamepedia\model\Commentaire;

require '../../../../vendor/autoload.php';

DBConnection::getInstance();

// créer 2 utilisateurs, 3 commentaires par utilisateurs, tous concernant le jeu 12342

/*
Utilisateur::create(['nom' => "Dupont", 'prenom' => "Jean", 'email' => "jean.dupont@email.fr", 'adresse_detaillee' => "42 Rue des écoles 54000 Nancy", 'numero_tel' => "0642424242", 'date_naissance' => "24/01/2000"])->save();
Utilisateur::create(['nom' => "Martin", 'prenom' => "Olivier", 'email' => "olivier.martin@email.fr", 'adresse_detaillee' => "18 Place du village 54000 Nancy", 'numero_tel' => "0600000000", 'date_naissance' => "15/05/1990"])->save();


Commentaire::create(['titre' => "Mon premier commentaire", 'contenu' => "Blablabla blabla blablablabla blabla.", 'date_creation' => "20/03/2017", 'id_game' => 12342, 'id_utilisateur' => 1])->save();
Commentaire::create(['titre' => "Mon deuxième commentaire", 'contenu' => "Blablabla blabla blabla", 'date_creation' => "21/03/2017", 'id_game' => 12342, 'id_utilisateur' => 1])->save();
Commentaire::create(['titre' => "Mon premier commentaire", 'contenu' => "Blobloblo bloblobloblo bloblo bloblobloblo.", 'date_creation' => "19/03/2017", 'id_game' => 12342, 'id_utilisateur' => 2])->save();
*/


echo ('Donnees deja creees');