<?php

use gamepedia\model\DBConnection;
use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\model\Utilisateur;
use gamepedia\model\Commentaire;


require '../../../../vendor/autoload.php';

DBConnection::getInstance();

echo '<meta charset="UTF-8">';

DB::enableQueryLog();

$user= Utilisateur::get();


foreach($user as $u){
	$c=$u->commentaire()->get()->count();
	if($c > 5){
		echo ($u->id . " " .$u->nom . " " .$u->prenom . '<br>');
	}
}


$query = DB::getQueryLog();
$lastQuery = end($query);

