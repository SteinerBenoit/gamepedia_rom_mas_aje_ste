<?php

use gamepedia\model\DBConnection;
use gamepedia\model\Compagnie;

require '../../../../vendor/autoload.php';

DBConnection::getInstance();

// les jeux dont le nom débute par Mario, publiés par une compagnie dont le nom contient "Inc." et dont le rating initial contient "3+"

$c = Compagnie::where( 'name', 'like', '%Inc.%' )->get();

foreach ( $c as $item ) {
	$g = $item->game()->where( 'name', 'like', 'Mario%' )->get();
	foreach($g as $item2){
		$r = $item2->gameRating()->where('name','like','%3+%')->get();
		foreach ($r as $item3){
			echo($item2->name . " || " . $item->name . " || " . $item3->name . "<br>");
		}
	}
}
