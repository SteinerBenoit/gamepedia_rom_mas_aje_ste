<?php
use gamepedia\model\DBConnection;
use gamepedia\model\Genre;

require '../../../../vendor/autoload.php';

DBConnection::getInstance();

// Ajout d'un nouveau genre
$existeDeja = Genre::where( 'name', 'like', 'Programmation' )->get()->count();

if(! $existeDeja){
	$genre = new Genre();
	$genre->name = 'Programmation';
	$genre->save();
	$genre->game()->attach(12);
	$genre->game()->attach(56);
	$genre->game()->attach(345);
	echo 'Genre Programmation ajoute !';
}else {
	echo 'Pas d\'ajout de genre !';
}




