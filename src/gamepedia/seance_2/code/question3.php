<?php

use gamepedia\model\DBConnection;
use gamepedia\model\Compagnie;

require '../../../../vendor/autoload.php';

DBConnection::getInstance();

echo('Les jeux developpes par une compagnie dont le nom contient Sony : <br> <br>');

$list = Compagnie::where( 'name', 'like', '%Sony%' )->get();

foreach ( $list as $l ) {
	$res= $l->game()->get();
	foreach($res as $resEncore){
		echo($resEncore->name . "<br>");
	}
}

