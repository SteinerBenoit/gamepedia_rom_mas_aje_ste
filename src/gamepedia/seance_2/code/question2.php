<?php

use gamepedia\model\DBConnection;
use gamepedia\model\Game;

require '../../../../vendor/autoload.php';

DBConnection::getInstance();

//les personnages des jeux dont le nom (du jeu) débute par 'Mario'

$g = Game::where( 'name', 'like', 'Mario%' )->get();
echo('Voici la liste des personnages des jeux commencant le mot Mario :<br>');

foreach($g as $item){
	$p = $item->personnage()->get();
	foreach($p as $pers){
		echo($pers->name . '<br>');
	}
	
}



