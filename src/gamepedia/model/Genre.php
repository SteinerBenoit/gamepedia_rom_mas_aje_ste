<?php

namespace gamepedia\model;

class Genre extends \Illuminate\Database\Eloquent\Model{

	protected $table="genre";
	protected $primaryKey = 'id' ;
	public $timestamps = false;


	public function	game() {
		return $this->belongsToMany('\gamepedia\model\Game', 'game2genre', 'game_id', 'genre_id') ;
	}
}