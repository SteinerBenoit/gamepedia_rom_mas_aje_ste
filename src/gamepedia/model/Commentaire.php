<?php

namespace gamepedia\model;

class Commentaire extends \Illuminate\Database\Eloquent\Model{
	
	protected $table='commentaire';
	protected $primaryKey = 'id';
	protected $fillable = ['titre', 'contenu', 'date_creation', 'id_game', 'id_utilisateur'];
	public $timestamps = true;
	
	
	
	public function utilisateur(){
		return $this->belongsTo('gamepedia\model\Utilisateur', 'id_utilisateur');
	}
	
}