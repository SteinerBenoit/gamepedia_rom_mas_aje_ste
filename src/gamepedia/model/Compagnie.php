<?php

namespace gamepedia\model;

class Compagnie extends \Illuminate\Database\Eloquent\Model{
	
	protected $table="company";
	protected $primaryKey = 'id' ;
	public $timestamps = false;
	
	public function	game() {
		return $this->belongsToMany('gamepedia\model\Game', 'game_developers', 'comp_id', 'game_id');
	}
	
}