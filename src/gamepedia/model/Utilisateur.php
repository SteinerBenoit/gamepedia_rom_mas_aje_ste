<?php

namespace gamepedia\model;

class Utilisateur extends \Illuminate\Database\Eloquent\Model{
	
	protected $table='utilisateur';
	protected $primaryKey = 'id';
	protected $fillable = ['nom', 'prenom', 'email', 'adresse_detaillee', 'numero_tel', 'date_naissance'];
	public $timestamps = false;
	

	
	public function	commentaire() {
		return $this->hasMany('gamepedia\model\Commentaire', 'id_utilisateur');
	}
	
	
}