<?php

namespace gamepedia\model;

class GameRating extends \Illuminate\Database\Eloquent\Model{
	
	protected $table='game_rating';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
	public function	game() {
		return $this->belongsToMany('gamepedia\model\Game', 'game2rating', 'rating_id', 'game_id');
	}

	public function	ratingBoard() {
		return $this->belongsTo('gamepedia\model\RatingBoard', 'rating_board_id');
	}
	
}
