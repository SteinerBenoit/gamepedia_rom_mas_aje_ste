<?php

namespace gamepedia\model;

class RatingBoard extends \Illuminate\Database\Eloquent\Model{
	
	protected $table='rating_board';
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function	game_rating() {
		return $this->hasMany('gamepedia\model\Game_rating', 'rating_board_id');
	}
	
}