<?php

namespace gamepedia\model;

class Game extends \Illuminate\Database\Eloquent\Model{
	
	protected $table='game';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
	public function	personnage() {
		return $this->belongsToMany('gamepedia\model\Personnage', 'game2character', 'game_id', 'character_id');
	}
	
	public function	first_appeared_in_game() {
		return $this->hasMany('gamepedia\model\Personnage', 'first_appeared_in_game_id');
	}
		
	public function	gameRating() {
		return $this->belongsToMany('gamepedia\model\GameRating', 'game2rating', 'game_id', 'rating_id');
	}
	
	public function	compagnie() {
		return $this->belongsToMany('gamepedia\model\Compagnie', 'game_developers', 'comp_id', 'game_id');
	}
	
	public function	platform() {
		return $this->belongsToMany('gamepedia\model\Plateforme', 'game2platform', 'game_id', 'platform_id');
	}
}