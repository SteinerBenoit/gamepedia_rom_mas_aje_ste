<?php

namespace gamepedia\model;

use Illuminate\Database\Capsule\Manager as DB;

class DBConnection{
	private static $_instance = null;
	
	private function __construct() {
		$config=parse_ini_file('../../../conf/conf.ini');
		$db = new DB();
		$db->addConnection($config);
		$db->setAsGlobal();
		$db->bootEloquent();
	}
	
	public static function getInstance() {
	
		if(is_null(self::$_instance)) {
			self::$_instance = new DBConnection();
		}
	
		return self::$_instance;
	}
	
}