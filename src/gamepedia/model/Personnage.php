<?php

namespace gamepedia\model;

class Personnage extends \Illuminate\Database\Eloquent\Model{
	
	protected $table="character";
	protected $primaryKey = 'id' ;
	public $timestamps = false;
	
}